import { createHash } from 'https://deno.land/std@0.79.0/hash/mod.ts';

/**
 * @function pwnedPassword
 * @param {String} password
 * @returns {Promise<Number>} breached count
 */
const pwnedPassword = async password => {
    const
        hash = createHash('sha1').update(password).toString().toUpperCase(),
        match = (await (await fetch(`https://api.pwnedpasswords.com/range/${hash.slice(0, 5)}`)).text())
            .match(new RegExp(`^${hash.slice(5)}:([0-9]+)$`, 'm'));
    return match ? parseInt(match[1]) : 0;
};

export {
    pwnedPassword
}

/*
Copyright © KaKi87
Permission is hereby granted, free of charge, to any person obtaining a copy of this software
and associated documentation files (the “Software”), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
The Software is provided “as is”, without warranty of any kind, express or implied,
including but not limited to the warranties of merchantability, fitness for a particular purpose and noninfringement.
In no event shall the authors or copyright holders be liable for any claim, damages or other liability,
whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the Software.
*/

/*
CHANGELOG
- 1.0.0 (2020-11-28) : Initial release
*/